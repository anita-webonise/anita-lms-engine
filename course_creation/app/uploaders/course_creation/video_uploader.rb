# encoding: utf-8
module CourseCreation
  # Video Uploader
  class VideoUploader < CarrierWave::Uploader::Base
    storage :file
    include CarrierWave::Video::Thumbnailer
    # Override the directory where uploaded files will be stored.
    # This is a sensible default for uploaders that are meant to be mounted:
    def store_dir
      "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
    end

    # Add a white list of extensions which are allowed to be uploaded.
    # For images you might use something like this:
    def extension_white_list
      %w(mp4)
    end

    version :thumb do
      process thumbnail: [{ format: 'png', quality: 10,
                            size: 580, strip: true, logger: Rails.logger }]
      def full_filename(for_file)
        png_name for_file, version_name
      end
    end

    def png_name(for_file, version_name)
      "#{version_name}_#{for_file.chomp(File.extname(for_file))}.png"
    end

    def fix_content_type
      file.content_type = 'image/png' if file
    end
  end
end
