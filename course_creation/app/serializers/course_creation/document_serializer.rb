module CourseCreation
  # Document Serializer
  class DocumentSerializer < ActiveModel::Serializer
    attributes :id, :title, :file

    def file
      object.file.present? && object.file.url
    end
  end
end
