module CourseCreation
  # UserCourseAssessments Serializer
  class UserCourseAssessmentsSerializer < ActiveModel::Serializer
    attributes :user_id
    has_many :course_assessments, serializer: UserCourseAssessmentSerializer
  end
end
