module CourseCreation
  # Question Serializer
  class QuestionSerializer < ActiveModel::Serializer
    attributes :id, :title, :additional_text, :question_category,
               :question_category_id, :status
    has_one :question_category
    has_many :answers, serializer: AnswerSerializer

    def answers
      object.answers.order(:id)
    end

    def question_category
      object.question_category.name
    end

    def status
      if context[:attempted].where(id: id).present?
        ApiStatuses::STATUS[2]
      else
        ApiStatuses::STATUS[0]
      end
    end
  end
end
