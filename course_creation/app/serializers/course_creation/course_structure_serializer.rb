module CourseCreation
  # CourseStructure Serializer
  class CourseStructureSerializer < ActiveModel::Serializer
    attributes :id, :name, :version_id, :parent_id, :content,
               :course_order, :chapter_order
    has_many :contents, serializer: CourseStructureSerializer

    def contents
      object.contents.rank(:chapter_order)
    end
  end
end
