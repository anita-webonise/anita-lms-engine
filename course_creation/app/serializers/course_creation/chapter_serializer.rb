module CourseCreation
  # Serializer for chapter details API
  class ChapterSerializer < ActiveModel::Serializer
    attributes :id, :name, :content, :resources, :contents

    def resources
      ChapterResourcesSerializer.new(object, scope: scope, root: false)
    end

    def contents
      object.contents.rank(:chapter_order).map do |section|
        SectionSerializer.new(section, scope: scope, root: false)
      end
    end
  end
end
