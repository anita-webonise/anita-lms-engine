module CourseCreation
  # Slide serializer
  class SlideSerializer < ActiveModel::Serializer
    attributes :id, :title, :number_of_columns, :slides_order, :settings,
               :contents

    def settings
      SlideSettingSerializer.new(object.slide_setting, root: false)
    end

    def contents
      object.slide_contents.order(:orientation).map do |content|
        SlideContentsSerializer.new(content, root: false)
      end
    end
  end
end
