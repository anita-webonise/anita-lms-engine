module CourseCreation
  # UserCourseAssessment Serializer
  class UserCourseAssessmentSerializer < ActiveModel::Serializer
    attributes :id, :chapter_name, :status, :name, :content,
               :content_type, :course_progress
    has_one :content_details

    def id
      object.assessment.course_section_id
    end

    def status
      course_progress.blank? ? ApiStatuses::STATUS[1] : course_progress.status
    end

    def course_progress
      user_course_progress =
        CourseTaking::UserCourseProgress.find_by_content_id_and_user_course_id(
          object.id, object.user_course_id)
    end

    def content
      object.assessment.course_section.content
    end

    def content_type
      object.assessment.course_section.name.split(':').first.delete(' ') if
        object.assessment.course_section.present?
    end

    def name
      object.assessment.course_section.name
    end

    def chapter_name
      return nil unless object.assessment.course_section.chapter
      object.assessment.course_section.chapter.name
    end

    def content_details
      object.assessment && AssessmentSerializer.new(
        object.assessment, root: false, context: { user_course: user_course })
    end

    def user_course
      CourseTaking::UserCourse.find(object.user_course_id)
    end
  end
end
