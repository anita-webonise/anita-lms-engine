module CourseCreation
  # presentation serializer
  class PresentationSerializer < ActiveModel::Serializer
    attributes :id, :title, :course_section_id, :content_data

    def content_data
      object.slides.order(:slides_order).map do |slide|
        SlideSerializer.new(slide, scope: scope, root: false)
      end
    end
  end
end
