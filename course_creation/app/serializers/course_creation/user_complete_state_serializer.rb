module CourseCreation
  # User initial state for course progress
  class UserCompleteStateSerializer < ActiveModel::Serializer
    attributes :chapter, :section, :content

    def chapter
      nil
    end

    def section
      nil
    end

    def content
      nil
    end
  end
end
