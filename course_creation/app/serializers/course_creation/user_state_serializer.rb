module CourseCreation
  # User last state serializer
  class UserStateSerializer < ActiveModel::Serializer
    attributes :chapter, :section, :content

    def chapter
      chapter = []
      object.map do |c|
        chapter.push(c.section.id) unless c.section.try(:parent_id)
      end
      chapter.blank? ? nil : chapter.last
    end

    def section
      section = []
      object.map do |c|
        section.push(c.section.id) if c.section.try(:parent_id) &&
                                      !c.section.try(:content)
      end
      section.blank? ? nil : section.last
    end

    def content
      content = []
      object.map do |c|
        content.push(c.section.id) if c.section.content && c.status == 1
      end
      content.blank? ? nil : content.last
    end
  end
end
