module CourseCreation
  # Resource Serializer
  class ChapterResourcesSerializer < ActiveModel::Serializer
    attributes :documents, :videos

    def documents
      object.resources.where(type: 'CourseCreation::Document').rank(
        :chapter_order).map do |document|
        DocumentSerializer.new(document, scope: scope, root: false)
      end
    end

    def videos
      object.resources.where(type: 'CourseCreation::Video').rank(
        :chapter_order).map do |video|
        VideoSerializer.new(video, scope: scope, root: false)
      end
    end
  end
end
