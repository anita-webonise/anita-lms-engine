module CourseCreation
  # Action Helper
  module ApplicationHelper
    def set_controller_scope
      controller.controller_name.to_sym
    end

    def localize(key)
      scope = [:course_creation] << set_controller_scope
      I18n.t(key, scope: scope)
    end

    def action_name
      controller.action_name
    end

    def parse_json(option)
      JSON.parse(option)
    rescue
      false
    end

    def show_errors(object, field_name)
      return if object.errors.blank? &&
                object.errors.messages[field_name].blank?
      field_name.to_s.capitalize.concat(
        ' ' + object.errors.messages[field_name].join(','))
    end
  end
end
