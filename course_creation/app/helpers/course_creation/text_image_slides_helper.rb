module CourseCreation
  # Text Image slide helper
  module TextImageSlidesHelper
    def image_path(slide)
      slide.image? ? slide.image.url : ''
    end

    def title_name(slide)
      slide.title.present? ? slide.title : localize(
        'text_slide.add_slide_name_placeholder')
    end

    def slide_description(slide)
      slide.description.present? ? slide.description : localize(
        'text_slide.add_text_content_placeholder')
    end

    def tab_title_name(slide)
      slide.title.present? ? slide.title : localize('text_slide.add_item')
    end
  end
end
