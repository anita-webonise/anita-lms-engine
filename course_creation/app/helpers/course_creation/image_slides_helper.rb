module CourseCreation
  #  Image and text image slide helper
  module ImageSlidesHelper
    def image_path(slide)
      slide.image? ? slide.image.url : ''
    end

    def title_name(slide)
      slide.title.present? ? slide.title : localize('text_slide.add_slide')
    end

    def slide_description(slide)
      if slide.description.present?
        slide.description
      else
        localize('text_slide.add_text_content_placeholder')
      end
    end
  end
end
