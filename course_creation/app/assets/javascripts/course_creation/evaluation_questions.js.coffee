$ ->
  $('.eval-question-list').sortable(
    items: '.evaluation-question'
    dropOnEmpty: true

    update: (e, ui) ->
      question_id = ui.item.data('question-id')
      version_id = ui.item.data('version-id')
      position = ui.item.index()
      $.ajax(
        type: 'PUT'
        url: Routes.course_creation_version_evaluation_question_path(version_id, question_id)
        dataType: 'json'
        data: { evaluation_question: { version_id: version_id, course_order_position: position } }
      )
  )

  $('.active-button').click ->
    question_id = $(@).data('id')
    version_id = $(@).data('version-id')
    active = $(@).val()
    $.ajax(
      type: 'PUT'
      url: Routes.course_creation_version_evaluation_question_path(version_id, question_id)
      dataType: 'json'
      data: { evaluation_question: { active: active } }
    )
