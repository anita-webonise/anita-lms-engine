#= require course_creation/jquery.form
#= require course_creation/colpick.js
$ ->
  $('.image-file-field').toggle($('#deleteImage').hasClass('hidden-area'))

  $('.slide-list').sortable(
    items: '.slide-box'
    update: (e, ui) ->
      ids = $('#ids').data()
      position = ui.item.index()
      $.ajax(
        type: 'put'
        dataType: 'json'
        url: Routes.course_creation_version_course_section_presentation_slide_path(ids.version, ids.csId, ids.presentationId, ids.id)
        data: { slide: { slides_order_position: position } }
      )
      total = $('.slide-index').length
      $('.slide-index').each (i) ->
        if i < total-1
          $(@).text(++i)
  )

  $('#color').colpick(
    layout:'hex'
    submit:1
    colorScheme:'dark'
    onSubmit: (hsb, hex, rgb, el, bySetColor) ->
      if !bySetColor
        $('#bg_color').val ("##{hex}")
      $(el).colpickHide()
      loader true
      form = $('.background-color').closest('form')
      form.ajaxSubmit
        url: 'update_settings'
        type: 'put'
        success: (data, status) ->
          $('.column-wrapper').css 'background-color', "##{hex}"
      loader false
      return
    onChange:(hsb, hex, rgb, el, bySetColor) ->
      $('.color-pick').css 'background-color', "##{hex}"
  ).keyup ->
    $(@).colpickSetColor @value
    return
  $('.colum-heading .slide-title').hover ->
    $(@).toggleClass 'addborder'
    return

  $('.slide-title-panel .slide-title').click ->
    $('#titleEditorWrapper').show()
    $(@).hide()
    return

  $('#titleEditorWrapper .cancel').click ->
    $('#titleEditorWrapper').hide()
    $('.slide-title-panel .slide-title').show()
    return

  displayVideo = (elementID,settings) ->
    defaults =
      height: 315
      width: 590
      swfPath: '/jwplayer/player.swf'
      controls: false
      autostart: true
    settings = $.extend({}, defaults, settings)
    settings['width'] = '100%' if settings['responsive']
    if $("##{elementID}").length > 0
      jwplayer(elementID).setup
        flashplayer: settings['swfPath']
        controlbar: 'none'
        allowscriptaccess: 'always'
        file: settings['url']
        provider: 'http'
        height: settings['height']
        width: settings['width']
        autostart: settings['autostart']

  $('#applySettings').click ->
    if $('#applySettings').prop('checked')
      slide = $('#ids').data()
      $.ajax
        method: 'put'
        url: Routes.course_creation_apply_seetings_version_course_section_presentation_slide_path(slide.version, slide.csId, slide.presentationId, slide.id)
        success: (data, status) ->
          if data.status is 200
            $('#apply_settings').show()
          else
            $('#error_apply_settings').show()

  $('.video').each ->
    index = $(@).data('index')
    video_url = $("#video_url_#{index}").val()
    displayVideo("videoWrap_#{index}",
                {url: video_url,
                controls: true,
                autostart: false,
                height: '250',
                width: '100%'})
  if $('.slide-heading h4').length
    $('.slide-heading h4').text($('.active-slide').data('index'))
