#= require tinymce-jquery
$ ->
  # Client side validation for assessment
  Types = {test: 'Test', quiz: 'Quiz', practice: 'Practice'}
  selected_type = $('#assessment_assessment_type').val()
  element = if selected_type is Types['practice'] then $('#testQuizCheck') else $('#testAssessmentRadio')
  status = selected_type is Types['test'] || selected_type is Types['quiz'] || selected_type is Types['practice']
  element.toggle(status)
  if $('#new_assessment').length
    $('#new_assessment').validate()
    $('#assessment_name').rules 'add',
      required: true
      messages: required: $('.submit-form').data('message')

    $('#assessment_assessment_type').rules 'add',
      required: true
      messages: required: $('.submit-form').data('message')

  $('#assessment_assessment_type').change ->
    $('#testAssessmentRadio').toggle(@value is Types['test'] || @value is Types['quiz'])
    $('#testQuizCheck').toggle(@value is Types['practice'])

  $('#passingCriteriaYes').click ->
    $('#passingCriteria').show()

  removePassingCriteria = ->
    $('#assessment_passing_percentage').val('')
    $('#assessment_no_of_displayed_questions').val('')

  $('#passingCriteriaNo').click ->
    $('#passingCriteria').hide()
    removePassingCriteria()

  assessment_type = $.trim($('.assessment-type-result').text())
  if assessment_type
    $('.control-group.assessment-checkbox').show()

  $('#edit_assessment').submit ->
    if $('#passingCriteriaNo').attr('checked') is 'checked'
      removePassingCriteria()

  assessment = $('#assessment_type').text()
  if assessment
    trim_assessment = $.trim(assessment.toLowerCase())
    assessment_selector = ".#{trim_assessment}"
    $(assessment_selector).show()
    assessment_selector = "##{trim_assessment}"
    $(assessment_selector).show()
  if $('#passingCriteriaNo').attr('checked') is 'checked'
    $('#passingCriteria').hide()

  tinyMCE.init
    selector: 'textarea.form-control:not(.no-mce)'
    plugins: [
      'textcolor colorpicker'
    ]
    toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | forecolor backcolor '
