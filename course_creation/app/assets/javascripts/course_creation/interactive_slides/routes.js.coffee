//= require 'course_creation/interactive_slides/interactive_common'
//= require 'course_creation/interactive_slides/views/text_interactive/text_interactive'
class Router
  constructor: ->
    _events()

  _events = ->
    commonInteractive = new lms.views.interactive()
    text_slide = new lms.views.textInterSlide()
    $('.update-links').off().on 'click', '#saveTitle', (event) ->
      if $('.selected').attr('data-id') != ''
        text_slide.edit_title()
      else
        $('.update-links').hide()
        event.preventDefault()
        text_slide.save_data()

    $('.update-links').on 'click', '.cancel', (event) ->
      $('.update-heading').find('h4').show()
      $('#title').val('')
      text_slide.cancel_title()

    $('.edit-slide-content').on 'click', '#saveContent', (event) ->
      text_slide.edit_data()

    $('.text-area-container .is-slide-desc').click ->
      $(@).hide().next('.edit-slide-content').show()
      if $('.selected').length && ($('.selected').attr('data-description') == $('.edit-slide-content').attr('data-description-content'))
        if $('#description').length
          tinyMCE.get('description').setContent('')
        else
          tinyMCE.get('image_slide_description').setContent('')
      else if !($('.selected').attr('data-description') == $('.edit-slide-content').attr('data-description-content'))
        if $('#description').length
          tinyMCE.get('description').setContent($('.selected').attr('data-description'))
        else
          tinyMCE.get('text_image_slide_description').setContent('')
      else if $('.ui-state-active > .ui-tabs-anchor').attr('data-description') == $('#text_image_slide_description').attr('placeholder')
        tinyMCE.activeEditor.setContent('')
      else
        tinyMCE.activeEditor.setContent($('.ui-state-active > .ui-tabs-anchor').attr('data-description'))

    $('.text-area-container').on 'click', '.cancel', (event) ->
      commonInteractive.reverseToogleClasses('edit-slide-content', 'text-area-container .is-slide-desc')

    $('.edit-quiz-name .cancel').click ->
      commonInteractive.toggleClasses('quiz-name', 'edit-quiz-name')

    $('.update-heading').click ->
      $('.edit-heading.hidden-area').show()
      $('.update-links').show()
      if $('.update-heading h4').text() == $('.edit-heading').attr('data-slide-title')
        $('.edit-heading').find('#title').val()
      else
        $('.edit-heading').find('#title').val($('.update-heading h4').text())

    $('.custom-upload').on 'click', ->
      $('.update-text-container').show()
      $('#deleteContent').show()
      $('.custom-upload').removeClass('selected')
      $(@).addClass('selected')
      $('.update-heading h4').show().text($('.add-section-container').find('.selected').attr('data-title'))
      $('.text-area-container').find('.is-slide-desc').show().html($('.add-section-container').find('.selected').attr('data-description'))
      $('.edit-heading').hide()
      $('.edit-slide-content.hidden-area').hide()

    $('#deleteContent').on 'click', ->
      confirmation = confirm($('.delete-section').attr('data-alert-message'));
      text_slide.delete_data(confirmation)

    if $('#text_slide_information').val() != $('#text_slide_minimum').val()
      $('.update-text-container').hide()
      $('#deleteContent').hide()
do ->
  router = new Router()
