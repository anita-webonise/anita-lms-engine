$ ->
  courseSectionOrder = (item)->
    $(item).each ->
      index = $(@).index()
      label = item.substring(1).charAt(0).toUpperCase() + item.substring(2)
      if item is '.chapter'
        $(@).find(".cs-label[data-cs-id=#{$(@).data('chapter-id')}]").html("#{label} #{index + 1}: ")
      else if $(@).data('content')?
        return null
      else
        contents = 0
        $(@).prevAll().each ->
          contents += 1 if $(@).data('content')?
        section_index = index - contents + 1
        $(@).find('.cs-label').html("#{label} #{section_index}: ")

  resetLabelOrder =->
    courseSectionOrder('.chapter')
    courseSectionOrder('.section')

  window.contentClick = (content_button = '.add-content')->
    $(content_button).click ->
      cs = $(this).data('cs-id')
      link = $(this)
      $("#cs-content-tab-#{cs}").toggle ->
        if $(this).is(':visible')
          link.html($('.cancel-text').html())
        else
          link.html($('.add-text').html())

  window.loadSyllabus =->
    $('#chapter-list').sortable(
      items: '.chapter'
      update: (e, ui) ->
        chapter_id = ui.item.data('chapter-id')
        version_id = $('#version_id').val()
        position = ui.item.index()
        $.ajax(
          type: 'PUT'
          url: Routes.course_creation_version_course_section_path(version_id, chapter_id)
          dataType: 'json'
          data: { course_section: { course_order_position: position } }
        )
        courseSectionOrder('.chapter')
    )

    $('.section-list').sortable(
      items: '.section'
      connectWith: '.section-list, .content-listing'
      dropOnEmpty: true
      placeholder: 'sortable-placeholder'
      update: (e, ui) ->
        section_id = ui.item.data('section-id')
        section_id ||= ui.item.data('content-id')
        chapter_object = ui.item.parent().parent()
        chapter_id = chapter_object.data('chapter-id')
        version_id = $('#version_id').val()
        position = ui.item.index()
        $.ajax(
          type: 'PUT'
          url: Routes.course_creation_version_course_section_path(version_id, section_id)
          dataType: 'json'
          data: { course_section: { parent_id: chapter_id, chapter_order_position: position } }
        )
        courseSectionOrder('.section')
    )

    $('.content-listing').sortable(
      items: '.course-content'
      connectWith: '.content-listing'
      dropOnEmpty: true
      placeholder: 'content-listing sortable-placeholder'
      update: (e, ui) ->
        content_id = ui.item.data('content-id')
        content_id ||= ui.item.data('section-id')
        section_object = ui.item.parent().parent()
        movable = ui.item.data('movable')
        movable ||= (movable is undefined)
        section_id = section_object.data('section-id')
        section_id ||= section_object.data('chapter-id')
        version_id = $('#version_id').val()
        position = ui.item.index()
        if movable
          $.ajax(
            type: 'PUT'
            url: Routes.course_creation_version_course_section_path(version_id, content_id)
            dataType: 'json'
            data: { course_section: { parent_id: section_id, chapter_order_position: position } }
          )
        else
          alert(ui.item.data('msg'))
          $(ui.sender).sortable('cancel')
          resetLabelOrder()
    )

    contentClick()
    resetLabelOrder()

  loadSyllabus()
