'use strict'

angular.module('CourseTakingApp').factory 'CourseEvaluation', [
  'restmod'
  'APP_CONSTANTS'
  (restmod, APP_CONSTANTS) ->
    return restmod.model APP_CONSTANTS.apiURL + '/evaluation_questions'
]
