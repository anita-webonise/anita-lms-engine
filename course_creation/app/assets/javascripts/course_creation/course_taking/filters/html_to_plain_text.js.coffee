'use strict'

angular.module('CourseTakingApp').filter 'htmlToPlaintext', ->
  (text) ->
    if text
      return angular.element(text).text()
    else
      return ''
