'use strict'

angular.module('CourseTakingApp').filter 'sanitize', [
  '$sce'
  ($sce) ->
    (htmlCode) ->
      $sce.trustAsHtml htmlCode
]
