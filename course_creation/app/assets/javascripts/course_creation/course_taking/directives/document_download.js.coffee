'use strict'

angular.module('CourseTakingApp').directive 'ctDownload', ->
  (scope, ele) ->
    element = angular.element(ele)
    element.click (e) ->
      fileUrl = element.attr('href')
      fileName = fileUrl.split('/').pop()
      extension = fileUrl.split('.').pop()
      if !window.ActiveXObject
        save = document.createElement('a')
        save.href = fileUrl
        save.target = '_blank'
        save.download = fileName or 'unknown'
        event = document.createEvent('Event')
        event.initEvent 'click', true, true
        save.dispatchEvent event
        (window.URL or window.webkitURL).revokeObjectURL save.href
      else if ! !window.ActiveXObject and document.execCommand
        _window = window.open(fileURL, '_blank')
        _window.document.close()
        _window.document.execCommand 'SaveAs', true, fileName or fileURL
        _window.close()
      false
    return
