'use strict'

angular.module('CourseTakingApp').directive 'ctAccordian', ->
  (scope, ele) ->
    element = angular.element(ele)
    element.click (e) ->
      isActive = if element.parent().next().hasClass('active') then true else false
      angular.element('.video-player').removeClass('active')
      if !isActive
        element.parent().next().addClass('active')
      false
    return
