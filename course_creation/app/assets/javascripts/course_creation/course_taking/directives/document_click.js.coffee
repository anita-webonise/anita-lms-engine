'use strict'

angular.module('CourseTakingApp').directive 'ctDocumentClick', [
  '$document'
  ($document) ->

    linkFunction = ->
      $document.on 'click', (event) ->
        event.stopImmediatePropagation()
        firstClassName = event.target.className.split(' ')[0]
        switch firstClassName
          when 'dropdown-toggle'
          else
            angular.element('.logout-wrap').find('.toggle').slideUp 'fast'
            angular.element('.welcome-link').removeClass 'active'
        return
      return

    linkFunction
]
