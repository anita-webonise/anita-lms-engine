'use strict'

angular.module('CourseTakingApp').directive 'ctPaginationSlider', ->
  (scope, ele) ->
    element = angular.element(ele)
    element.click (e) ->
      paginateItemWidth = angular.element('.pagination').find('li').outerWidth(true)
      listWidth = angular.element('.pagination ul').outerWidth(true)
      paginateWidth = angular.element('.pagination').outerWidth(true)
      diff = ( listWidth - paginateWidth ) + 1

      paginateListRight = angular.element('.pagination').find('ul').css('right')
      animateLeft = parseInt(paginateListRight) + paginateItemWidth
      animateRight = parseInt(paginateListRight) - paginateItemWidth

      if element.attr('data-value') == 'next' && !element.hasClass('disabled')
        angular.element('.pagination-prev').removeClass('disabled')
        angular.element('.pagination').find('ul').animate({right: animateLeft})
        if ((parseInt(paginateListRight) + paginateItemWidth) >= diff)
          element.addClass('disabled')

      if element.attr('data-value') == 'prev'  && !element.hasClass('disabled')
        angular.element('.pagination-next').removeClass('disabled')
        angular.element('.pagination').find('ul').animate({right: animateRight})
        if ((parseInt(paginateListRight) - paginateItemWidth) < paginateItemWidth)
          element.addClass('disabled')

    return
