'use strict'

angular.module('CourseTakingApp').controller 'InteractiveSlideContentCtrl', [
  '$scope'
  'CONTENT_STATUS'
  '$rootScope'
  'CONTENT_TYPES'
  '$timeout'
  'Loader'
  ($scope, CONTENT_STATUS, $rootScope, CONTENT_TYPES, $timeout, Loader) ->
    $scope.initInteractiveContent = ->
      $scope.currentSlideIndex = null
      $scope.startPage = true
      if $scope.currentContent.content_details.content_data
        $scope.slides = $scope.currentContent.content_details.content_data
        $scope.interactiveType = $scope.currentContent.content_details.interactive_slide_type
        $scope.slideName = $scope.currentContent.content_details.name
        $scope.slideDescription = $scope.currentContent.content_details.description

        angular.forEach $scope.slides, (slideImage, key) ->
          if $scope.currentContent.status == CONTENT_STATUS.COMPLETED && !$rootScope.$preview()
            slideImage.isSeen = true
          else
            slideImage.isSeen = false
      $timeout ->
        Loader.hide()

    $scope.selectSlide = (slideIndex, imageSlide) ->
      $scope.currentSlideIndex = slideIndex
      imageSlide.isSeen = true
      $scope.startPage = false
      isAllSeen = _.filter($scope.slides, (slide) ->
        slide.isSeen == true
      )
      if isAllSeen.length == $scope.slides.length
        angular.element('.content-next-btn').removeClass('disabled')
        return

    $scope.$on 'changeContent', ->
      if $scope.currentContentType == CONTENT_TYPES.INTERACTIVESLIDE
        $scope.initInteractiveContent()
    return
]
