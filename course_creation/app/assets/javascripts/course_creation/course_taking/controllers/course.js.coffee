'use strict'

angular.module('CourseTakingApp').controller 'CourseCtrl', [
  '$scope'
  'Course'
  '$stateParams'
  'sharedResources'
  '$rootScope'
  'CONTENT_STATUS'
  'PAGINATE_OFFSET'
  '$timeout'
  ($scope, Course, $stateParams, sharedResources, $rootScope, CONTENT_STATUS, PAGINATE_OFFSET, $timeout)->
    $scope.CONTENT_STATUS = CONTENT_STATUS
    courseId = $stateParams.courseId
    $rootScope.containers = []

    $scope.initCourseOutline = ->
      if courseId
        Course.$find(courseId).$then ((response) ->
          $scope.chapters = response.data.chapters
          sharedResources.resources = response.data.resources
          sharedResources.evaluationQuestions = response.data.evaluation_questions

          $rootScope.courseId = courseId
          $rootScope.courseName = response.data.title
          $rootScope.versionId = response.data.version_id
          if response.data.user_state
            $rootScope.currentChapterId = response.data.user_state.chapter
            $rootScope.currentSectionId = response.data.user_state.section
            $rootScope.currentContentId = response.data.user_state.content
            $rootScope.courseState = 'InProgress'

            if response.data.user_state.section
              $scope.selectContainer(response.data.user_state.section, true, CONTENT_STATUS.IN_PROGESS, response.data.user_state.chapter, true)
            else
              $scope.selectContainer(response.data.user_state.chapter, false, CONTENT_STATUS.IN_PROGESS, response.data.
                user_state.chapter, true)
            $rootScope.evalutionCompleted = false
            $rootScope.courseCompleted = false
          else if !response.data.evaluation_completed
            $rootScope.courseState = 'CourseEvalution'
            $rootScope.courseCompleted = true
            $rootScope.evalutionCompleted = false
            $timeout ->
              cta.setScreenHeight()
              cta.slideIn()
              return
          else if !response.data.user_state and $scope.chapters.length
            $rootScope.courseState = 'Congratulation'
            $rootScope.evalutionCompleted = true
            $rootScope.courseCompleted = true
            $timeout ->
              cta.setScreenHeight()
              cta.slideIn()
              return

          $rootScope.$broadcast 'sharedResourcesChanged'
        ), $scope.onError()

    $scope.selectContainer = (containerId, isSection, status, parentContainerId, isFirstLoad) ->
      if (status != CONTENT_STATUS.NOT_STARTED) or $rootScope.$preview()
        $rootScope.courseState = 'InProgress'
        if isSection
          $rootScope.currentChapterId = parentContainerId
          $rootScope.currentSectionId = containerId
          $rootScope.currentContainer.type = 'section'
          $rootScope.currentContainer.id = containerId
        else
          $rootScope.currentChapterId = containerId
          $rootScope.currentSectionId = null
          $rootScope.currentContainer.type = 'chapter'
          $rootScope.currentContainer.id = containerId

        if !isFirstLoad
          $rootScope.currentContentId = null

        if $rootScope.containers.length
          $rootScope.currentContainerIndex = _.findIndex $rootScope.containers, (container) ->
            container.containerId == $rootScope.currentContainer.id
        else
          angular.forEach $scope.chapters, (chapter) ->
            if chapter.has_contents
              obj =
                containerId: chapter.id
                isSection: false
                status: chapter.status
                parentContainerId: chapter.id
                isLastSection: if chapter.contents.length == 0 then true else false
              $rootScope.containers.push(obj)
              if chapter.id == $rootScope.currentContainer.id
                $rootScope.currentContainerIndex = $rootScope.containers.length - 1
            angular.forEach chapter.contents, (section, index) ->
              if (section.is_section && section.has_contents) or section.is_content
                obj =
                  containerId: section.id
                  isSection: true
                  status: section.status
                  parentContainerId: chapter.id
                  isLastSection: if index == chapter.contents.length - 1 then true else false
                $rootScope.containers.push(obj)
                if section.id == $rootScope.currentContainer.id
                  $rootScope.currentContainerIndex = $rootScope.containers.length - 1
        $rootScope.$broadcast 'containerChanged', isFirstLoad

    $scope.showCourseEvaluation = ->
      if !angular.element('.course-evaluation').hasClass('disabled')
        $rootScope.courseState = 'CourseEvalution'
        $rootScope.currentChapterId = null
        $rootScope.currentSectionId = null

    $scope.$on 'changeContainer', (evemt, offset) ->
      newContainer = $rootScope.currentContainerIndex + offset
      if newContainer >= 0 and newContainer <= $rootScope.containers.length - 1
        $scope.currentSlide = 0
        $scope.noOfSlides = 0
        currentContainer = $rootScope.containers[newContainer]
        if offset == PAGINATE_OFFSET.PREVIOUS || currentContainer.status == CONTENT_STATUS.COMPLETED || $rootScope.$preview()
          $scope.selectContainer(currentContainer.containerId, currentContainer.isSection, currentContainer.status, currentContainer.parentContainerId)
        else
          $scope.initCourseOutline()
      else if newContainer >= $rootScope.containers.length - 1 and $rootScope.containers.length
        $scope.initCourseOutline()
      else
        if !$rootScope.containers.length
          angular.forEach $scope.chapters, (chapter) ->
            if chapter.has_contents
              obj =
                containerId: chapter.id
                isSection: false
                status: chapter.status
                parentContainerId: chapter.id
                isLastSection: if chapter.contents.length == 0 then true else false
              $rootScope.containers.push(obj)
            angular.forEach chapter.contents, (section, index) ->
              if (section.is_section && section.has_contents) or section.is_content
                obj =
                  containerId: section.id
                  isSection: true
                  status: section.status
                  parentContainerId: chapter.id
                  isLastSection: if index == chapter.contents.length - 1 then true else false
                $rootScope.containers.push(obj)
          lastContainer = $rootScope.containers[$rootScope.containers.length-1]
          $scope.selectContainer(lastContainer.containerId, lastContainer.isSection, lastContainer.status, lastContainer.parentContainerId)
]
