'use strict'

angular.module('CourseTakingApp').controller 'DocumentContentCtrl', [
  '$scope'
  'CONTENT_TYPES'
  '$timeout'
  'Loader'
  ($scope, CONTENT_TYPES, $timeout, Loader) ->
    $scope.initDocumentContent = ->
      $scope.pdfUrl = $scope.currentContent.content_details.file
      return

    $scope.onLoad = ->
      $timeout (->
        Loader.hide()
        angular.element('.content-next-btn').removeClass('disabled')
      ), 2500

    $scope.$on 'changeContent', ->
      if $scope.currentContentType == CONTENT_TYPES.DOCUMENT
        $scope.initDocumentContent()
    return
]
