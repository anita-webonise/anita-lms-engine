$ ->

  $('#uploadFile').on 'change', ->
    files = @files || []
    return unless files.length or window.FileReader
    if /^image/.test(files[0].type)
      reader = new FileReader
      reader.readAsDataURL files[0]
      reader.onloadend = ->
        $('#upload_image').attr('src', @result)
        return
    return
  $('#uploadVideo').on 'change', ->
    $('.video-image-wrap .video-name').hide()
    return

# validate add and edit course form
  if $('.new_course, .edit_course').length
    $('.new_course, .edit_course').validate submitHandler: (form) ->
        loader true

      $('.edit_course').find('.upload').removeClass('required')
      elements = $('.new_course .required, .edit_course .required')

      elements.each ->
        $(this).rules 'add',
          required: true
          messages: required: $('#submit').data('message')
        return

      $('#uploadFile').rules 'add',
        extension: $('#uploadFile').data('type')
        messages:
          extension: $('#uploadFile').data('message')
      $('#uploadVideo').rules 'add',
        extension: $('#uploadVideo').data('type')
        messages:
          extension: $('#uploadVideo').data('message')

  $('.submit_form').on 'click', ->
    if $('.edit_course').length
      content = tinyMCE.get('course_versions_attributes_0_description').getContent()
      if content is ''
        $('.error').html($('#submit').data('message'))
        return false

  $('#add_category').on 'click', ->
    category_name = $('#category_name').val().trim()
    $.ajax(
      type: 'POST'
      url: Routes.course_creation_categories_path()
      dataType: 'json'
      data: { category: { name: category_name } }
      success: (data, status) ->
        $('.category-message').text(data.notice).show()
        $('.category-text #category_name').focusin ->
          $('.category-message').fadeOut 2000
        if data.status
          $('.category-message').attr('class', 'category-message success')
          $('#category_name').val('')
          new_option = "<option value = #{data.id}> #{data.name} </option>"
          $('#course_versions_attributes_0_category_id').append(new_option)
        else
          $('.category-message').attr('class', 'category-message error')
    )
