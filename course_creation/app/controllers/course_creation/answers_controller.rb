require_dependency "course_creation/application_controller"

module CourseCreation
  # Answer controller
  class AnswersController < CourseCreation::BaseController
    before_action :set_answer, only: [:destroy]

    # DELETE /answers/1
    def destroy
      if @answer.destroy
        render json: { status: 200 }, serializer: nil
      else
        render json: { status: 500 }, serializer: nil
      end
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_answer
      @answer = Answer.find_by_id(params[:id])
    end

  end
end
