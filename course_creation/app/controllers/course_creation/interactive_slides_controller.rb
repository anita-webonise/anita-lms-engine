require_dependency 'course_creation/application_controller'
module CourseCreation
  # InteractiveSlides Controller
  class InteractiveSlidesController < CourseCreation::BaseController
    before_action :set_version, :course_section
    def new
      @interactive_slide = InteractiveSlide.new
    end

    def create
      @interactive_slide = InteractiveSlide.new(interactive_slide_params)
      @interactive_slide = @course_section.build_interactive_slide(
        interactive_slide_params)
      @interactive_slide.version_id = @course_section.version_id
      if @interactive_slide.save
        @interactive_slide.sectionize_content
        set_flash_message :notice, :success
        slide_path
      else
        set_flash_message :notice, :error
        render :new
      end
    end

    def index
      @interactive_slides = InteractiveSlide.all
    end

    def update
      interactive_slide = InteractiveSlide.find(params[:id])
      return unless interactive_slide
      if interactive_slide.update_attributes(interactive_slide_params)
        interactive_slide.update_section
        render json: { data: interactive_slide,
                       status: true }
      else
        render json: { status: false }
      end
    end

    private

    def interactive_slide_params
      params.require(:interactive_slide).permit(
        :name, :description, :interactive_slide_type,
        :version_id, :course_section_id)
    end

    def slide_path
      inter_slide_type = @interactive_slide.interactive_slide_type
      case inter_slide_type
      when InteractiveSlide::TEXTSLIDE
        interactive_slide_route(InteractiveSlide::TEXTROUTE)
      when InteractiveSlide::IMAGESLIDE
        interactive_slide_route(InteractiveSlide::IMAGEROUTE)
      else
        interactive_slide_route(InteractiveSlide::TEXTIMAGEROUTE)
      end
    end

    def interactive_slide_route(format)
      path = "new_version_course_section_interactive_slide_#{format}_slide_path"
      redirect_to send(path,
                       @interactive_slide.version_id,
                       @interactive_slide.course_section_id,
                       @interactive_slide.id)
    end

    def set_version
      @version = Version.find_by_id(params[:version_id])
    end

    def course_section
      @course_section = CourseSection.find_by_id(params[:course_section_id])
    end
  end
end
