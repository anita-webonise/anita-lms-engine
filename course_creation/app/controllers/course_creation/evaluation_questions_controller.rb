require_dependency "course_creation/application_controller"

module CourseCreation
  class EvaluationQuestionsController < CourseCreation::BaseController
    before_action :set_evaluation_question, except: [:index, :create]
    before_action :set_version
    before_action :set_questions

    def index
      @question = EvaluationQuestion.new
    end

    def create
      @question = @version.evaluation_questions.new(evaluation_question_params)
      if @question.save
        index_redirect
      else
        render :index
      end
    end

    def edit
    end

    def update
      if @question.update(evaluation_question_params)
        index_redirect
      else
        render :index
      end
    end

    def destroy
      @question.destroy!
      index_redirect
    end

    private

    def set_evaluation_question
      @question = EvaluationQuestion.find(params[:id])
    end

    def set_questions
      @questions = @version.evaluation_questions.rank(:course_order)
    end

    def set_version
      @version = Version.find(params[:version_id])
    end

    def evaluation_question_params
      params.require(:evaluation_question).permit(:content, :active,
                                                  :course_order_position)
    end

    def index_redirect
      respond_to do |format|
        format.html { redirect_to version_evaluation_questions_path(@version) }
        format.json { render nothing: true }
      end
    end
  end
end
