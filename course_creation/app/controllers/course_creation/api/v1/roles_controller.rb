module CourseCreation
  module API
    module V1
      # roles controller for APi
      class RolesController < API::V1::BaseController
        before_action :set_role, only: :versions

        def versions
          @versions = @role.versions.where('published = ?', false)
          render json: @versions, each_serializer:
                 CourseListingSerializer, root: :courses,
                 'status' => success, meta_key: 'status'
        end

        private

        def role_params
          params.require(:id)
        end

        def set_role
          @role = Role.find(role_params)
        end
      end
    end
  end
end
