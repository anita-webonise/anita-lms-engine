module CourseCreation
  module API
    module V1
      # Course controller API
      class CoursesController < API::V1::BaseController
        before_action :set_course

        def show
          render json: @course,
                 serializer: CourseSerializer,
                 'status' => success, meta_key: 'status'
        end

        private

        def set_course
          @course = Course.find(course_params)
        end

        def course_params
          params.require(:id)
        end
      end
    end
  end
end
