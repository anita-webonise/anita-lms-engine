module CourseCreation
  module API
    module V1
      # Resource Controller
      class ResourcesController < API::V1::BaseController
        before_action :set_resources, only: :index
        before_action :set_resource, only: :show

        def index
          render json: @resources,
                 each_serializer: ResourceSerializer,
                 'status' => success, meta_key: 'status'
        end

        def show
          render json: @resource,
                 serializer: ResourceSerializer,
                 'status' => success, meta_key: 'status'
        end

        private

        def set_resources
          @resources =
            if section_params.present?
              CourseSection.find(section_params[:course_section_id]).resources
            elsif version_params.present?
              Version.find(version_params[:version_id]).resources
            end
          return render json: { status: params_missing } unless @resources
        end

        def set_resource
          @resource = Resource.find(resource_params)
        end

        def resource_params
          params.require(:id)
        end

        def section_params
          params.permit(:course_section_id)
        end

        def version_params
          params.permit(:version_id)
        end
      end
    end
  end
end
