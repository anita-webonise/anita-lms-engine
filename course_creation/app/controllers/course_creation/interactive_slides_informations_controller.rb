require_dependency 'course_creation/application_controller'
module CourseCreation
  # InteractiveSlide Information Controller
  class InteractiveSlidesInformationsController < CourseCreation::BaseController
    before_action :interactive_slide_information,
                  only: [:destroy, :edit, :update, :delete_slide_image]
    before_action :interactive_slide, only: [:destroy, :edit, :update,
                                             :delete_slide_image]
    before_action :slide_type
    before_action :set_version
    before_action :validate_image_slide, only: :delete_slide_image

    def new
      @interactive_slide_information = interactive_slide_info_class.new
      @interactive_slide = InteractiveSlide.find(
        params[:interactive_slide_id])
      @text_image_slides =
        @interactive_slide.interactive_slides_informations.where(
          type: 'CourseCreation::TextImageSlide').order(:id).to_a
      @image_slides =
        @interactive_slide.interactive_slides_informations.where(
          type: 'CourseCreation::ImageSlide').order(:id).to_a
      @text_slides =
        @interactive_slide.interactive_slides_informations.where(
          type: 'CourseCreation::TextSlide').order(:id).to_a
    end

    def create
      @interactive_slide_information = interactive_slide_info_class.new(
        interactive_slide_information_params)
      if @interactive_slide_information.save
        render json: {
          status: true, inter_slide: @interactive_slide_information
        }, serializer: nil
      else
        render json: { status: false }
      end
    end

    def update
      if @interactive_slide_info
         .update_attributes(interactive_slide_information_params)
        render json: { status: true, inter_slide:
                                    @interactive_slide_info }
      else
        render json: { status: false }
      end
    end

    def destroy
      if @interactive_slide_info.destroy
        render json: { status: true }
      else
        render json: { status: false }
      end
    end

    def delete_slide_image
      if @interactive_slide_info.update_attributes(remove_image: true)
        render json: { status: 200,
                       slide: @interactive_slide_info }, serializer: nil
      else
        render json: { status: 500 }, serializer: nil
      end
    end

    private

    def slide_type
      @type = if InteractiveSlidesInformation::TYPES.include?(params[:type])
                params[:type]
              else
                InteractiveSlidesInformation::INTERACTIVESLIDESINFORMATION
              end
    end

    def validate_image_slide
      @interactive_slide_info.destroy if
        @interactive_slide_info.title.nil? &&
        @interactive_slide_info.description.nil?
    end

    def interactive_slide_info_class
      CourseCreation.const_get(@type)
    end

    def interactive_slide_information_params
      params.require(@type.underscore).permit(
        :title, :description, :interactive_slide_id, :type, :image).merge(
          interactive_slide_id: params[:interactive_slide_id])
    end

    def interactive_slide
      @inter_slide = @interactive_slide_info.interactive_slide
    end

    def interactive_slide_information
      @interactive_slide_info =
        InteractiveSlidesInformation.find(params[:id])
    end

    def set_version
      @version = Version.find_by_id(params[:version_id])
    end
  end
end
