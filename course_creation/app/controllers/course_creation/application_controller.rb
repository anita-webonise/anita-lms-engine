module CourseCreation
  # Application base controller
  class ApplicationController < ActionController::Base
    # Sets the flash message with :key, using I18n. By default you are able
    # to setup your messages using specific resource scope, and if no message is
    # found we look to the default scope. Set the "now" options key to a true
    # value to populate the flash.now hash in lieu of the default flash hash (so
    # the flash message will be available to the current action instead of the
    # next action).
    # Example (i18n locale file):
    #
    #   en:
    #     course_creation:
    #       categories:
    #         #default_scope_messages - only if categories_scope is not found
    #         courses:
    #           #categories_scope_messages
    #
    # Please refer to en.yml locale file to check what messages are
    # available.

    def set_flash_message(key, kind, options = {})
      message = find_message(kind, options)
      if options[:now]
        flash.now[key] = message if message.present?
      else
        flash[key] = message if message.present?
      end
    end

    # Get message for given
    def find_message(kind, options = {})
      options[:scope] ||= translation_scope
      options[:default] = Array(options[:default]).unshift(kind.to_sym)
      options = course_creation_i18n_options(options)
      I18n.t("#{options[:scope]}.#{kind}", options)
    end

    # Controllers inheriting ApplicationController are advised to override this
    # method so that other controllers inheriting from them would use
    # existing translations.
    def course_creation_i18n_options(options)
      options
    end

    # Controllers inheriting ApplicationController are advised to override this
    # method so that other controllers inheriting from them would use
    # existing translations.
    def translation_scope
      "course_creation.#{controller_name}"
    end

    def render_notice(message)
      flash.now.notice = message
      respond_to do |format|
        format.js { render file: 'layouts/course_creation/notice' }
      end
    end

    # Routing error method called if parameters are missing,
    # action not found, routing error
    def routing_error
      respond_to do |format|
        format.json do
          render json: { status: { code: 404, message: 'Failure' } },
                 status: 400
        end
        format.html { render file: "#{Rails.root}/public/404.html" }
      end
    end
  end
end
