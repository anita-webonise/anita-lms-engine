module CourseCreation
  # Slide model
  class Slide < ActiveRecord::Base
    belongs_to :presentation
    has_many :slide_contents, dependent: :destroy
    has_one :slide_setting, dependent: :destroy
    accepts_nested_attributes_for :slide_contents, :slide_setting
    after_create :build_layout, :set_defaults
    include CourseCreation::VersionValidator
    validate :version_editable, on: :create, if: 'presentation.present?'
    before_destroy :version_editable
    include RankedModel

    ranks :slides_order,
          with_same: :presentation_id

    def build_layout
      return if slide_contents.count > 0
      if number_of_columns > 1
        slide_contents << [LeftColumn.new, RightColumn.new]
      else
        slide_contents << [CenterColumn.new]
      end
    end

    def set_defaults
      return if slide_setting
      create_slide_setting(background_color: '#ffffff', transition: 'slideNone')
    end

    def delete_column(column_id)
      column = SlideContent.find_by_id(column_id)
      return nil unless column
      orientation = column.orientation
      column.destroy
      slide_contents.create(orientation: orientation)
    end

    def clone_attributes
      new_slide = deep_clone include: [:slide_setting, :slide_contents]
      return nil unless new_slide
      if new_slide.slide_setting.background_img
        new_slide.slide_setting.background_img = slide_setting.background_img
      end
      slide_contents.each_with_index do |media, index|
        new_slide.slide_contents[index].file_url = media.file_url
      end
      new_slide
    end

    def apply_settings
      other_slides = presentation.slides - [self]
      attributes = slide_setting.attributes.slice(
        'background_color', 'transition')
      other_slides.each do |slide|
        slide.slide_setting.update_attributes(attributes)
        slide.slide_setting.background_img = slide_setting.background_img
        slide.save
      end
    end

    def valid_slide?
      title.present? || valid_slide_content?
    end

    def valid_slide_content?
      slide_contents.each do |slide|
        return false unless slide.content? || slide.file_url?
      end
      true
    end
  end
end
