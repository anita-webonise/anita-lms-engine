module CourseCreation
  # Slide content, STI implementation
  class SlideContent < ActiveRecord::Base
    belongs_to :slide
    self.inheritance_column = :orientation
    mount_uploader :file_url, SlideMediaUploader
  end
end
