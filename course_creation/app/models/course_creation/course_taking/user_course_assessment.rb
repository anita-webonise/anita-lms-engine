module CourseCreation
  module CourseTaking
    # User course assessment model
    class UserCourseAssessment < ActiveRecord::Base
      has_many :user_questions, dependent: :destroy, foreign_key: 'assessment_id'
      belongs_to :assessment, class_name: CourseCreation::Assessment
      belongs_to :user_course
      validates :assessment_id, :user_course_id, presence: true
      after_create :evaluate_assessment

      def evaluate_percentage
        questions = UserQuestion.where(
          'assessment_id = ? and user_course_id = ?',
          assessment_id, user_course_id)
        correct_questions = questions.where(passing_status: 1).count
        return 0 if questions.count == 0
        ((correct_questions.to_f / questions.count) * 100).to_i
      end

      def evaluate_assessment
        self.assessment_type = assessment.assessment_type
        self.assessment_percentage = evaluate_percentage
        self.status =
          assessment.passing_percentage <= assessment_percentage ? 1 : 0
        save
      end

      def delete_questions
        user_questions =
          CourseTaking::UserQuestion.where(
            'assessment_id = ? AND user_course_id = ?',
            assessment_id, user_course_id)
        user_questions.map(&:delete_answers)
        user_questions.destroy_all
      end

      def remove_assessment
        delete_questions
        CourseTaking::UserCourseAssessment.destroy(id)
      end
    end
  end
end
