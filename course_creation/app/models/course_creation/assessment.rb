# course creation
module CourseCreation
  # Assessment module
  class Assessment < ActiveRecord::Base
    include CourseContent
    # Removing test and practise(Assessment as quiz type was only
    # supportes { test: 'Test', quiz: 'Quiz', practice: 'Practice' })
    TYPES = { quiz: 'Quiz' }
    PASSING_CRITERIA_YES = true
    PASSING_CRITERIA_NO = false
    ASSESSMENT = 'Assessment'
    belongs_to :course_section
    has_many :questions, dependent: :destroy
    validates :name, presence: true
    validates_length_of :name, maximum: 50
    validates :assessment_type, presence: true
    validates :passing_percentage, inclusion: { in: 0..100, allow_nil: true,
                                                message: I18n.t(:percentage) }
    validates :no_of_displayed_questions, numericality: true, allow_nil: true
    include CourseCreation::VersionValidator
    validate :version_published, on: :create
    before_destroy :version_editable
    before_update :editable?

    def editable?
      return true if course_section.version.editable?
      if passing_criteria_changed? || assessment_type_changed? ||
         passing_percentage_changed? ||
         no_of_displayed_questions_changed?
        false
      else
        true
      end
    end

    def valid_assessment?
      questions.any? && (no_of_displayed_questions.to_i <= questions.count)
    end
  end
end
