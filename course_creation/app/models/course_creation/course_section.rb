module CourseCreation
  # Course Section model
  class CourseSection < ActiveRecord::Base
    belongs_to :version
    belongs_to :chapter, class_name: 'CourseSection', foreign_key: 'parent_id'
    has_many :contents, class_name: 'CourseSection', foreign_key: 'parent_id',
                        dependent: :destroy
    has_many :resources, dependent: :destroy

    has_one :content_resource, dependent: :destroy, class_name: 'Resource',
                               foreign_key: 'content_section_id'
    has_one :presentation, dependent: :destroy
    has_one :custom_content, dependent: :destroy
    has_one :interactive_slide, dependent: :destroy,
                                foreign_key: 'course_section_id'
    has_one :assessment, dependent: :destroy

    scope :chapters, -> { where(parent_id: nil) }

    validates :name, presence: true
    validates_length_of :name, maximum: 70
    CONTENT_TYPES = { presentation: 'presentation' }
    include RankedModel
    include ResourceCollection
    include CourseCreation::VersionValidator
    validate :version_editable, on: :create
    before_destroy :version_editable
    ranks :course_order,
          with_same: :version_id,
          scope: :chapters

    ranks :chapter_order,
          with_same: [:parent_id, :content]

    def section?
      parent_id.present? && !content
    end

    def sections
      contents.where(content: false)
    end

    def direct_contents
      contents.includes(:version, :content_resource, :presentation,
                        :interactive_slide, :custom_content, :assessment
                       ).where(content: true)
    end

    def section_content_present?
      content_resource.present? || validate_presentation? ||
        custom_content.present? || validate_assessment? ||
        valid_section? || interactive_slide.present?
    end

    def section_has_valid_contents?
      section_status = contents.any?
      if section_status
        contents.each do |sec|
          return false unless sec.section_content_present?
        end
      end
      section_status
    end

    def section_present?
      contents.each do |section|
        return false unless section.section_content_present?
      end
      true
    end

    def wrapped_content
      content_resource || presentation || custom_content || assessment ||
        interactive_slide
    end

    def validate_assessment?
      assessment.present? && assessment.valid_assessment?
    end

    def validate_presentation?
      presentation.present?
    end

    def valid_section?
      section? && section_has_valid_contents?
    end

    # course section cosider in course progress
    def valid_progress_content?
      content || is_assessment
    end
  end
end
