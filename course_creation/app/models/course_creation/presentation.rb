module CourseCreation
  # Presentation class
  class Presentation < ActiveRecord::Base
    has_many :slides, dependent: :destroy
    accepts_nested_attributes_for :slides
    validates :title, :course_section_id, presence: true
    belongs_to :course_section
    include CourseContent
    include CourseCreation::VersionValidator
    validate :version_editable, on: :create
    before_destroy :version_editable

    def valid_presentation?
      slides.each do |slide|
        return false unless slide.valid_slide?
      end
      true
    end
  end
end
