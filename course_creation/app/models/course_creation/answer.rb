module CourseCreation
  # Course Model
  class Answer < ActiveRecord::Base
    belongs_to :question
    acts_as_list scope: :question
    mount_uploader :image, ImageUploader
    include CourseCreation::VersionValidator
    validate :version_published, on: :create, if: 'question.present?'
    before_destroy :version_editable
  end
end
