FactoryGirl.define do
  factory :course_creation_user_courses,
          class: 'CourseCreation::CourseTaking::UserCourse' do |user_course|
    association :version,
                factory: :course_creation_version,
                strategy: :create
    user_course.progress_percentage Faker::Number.number(1)
  end
end
