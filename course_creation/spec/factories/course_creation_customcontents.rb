
FactoryGirl.define do
  factory :course_creation_customcontent, class:
           CourseCreation::CustomContent do
    name Faker::Name.name
    zip { Rack::Test::UploadedFile.new(File.join(CourseCreation::Engine.root, '/spec/fixtures/test.zip')) }
  end

  factory :invalid_course_creation_customcontent, class: 'CourseCreation::CustomContent' do
    name ''
    zip { Rack::Test::UploadedFile.new(File.join(CourseCreation::Engine.root, '/spec/fixtures/test.txt')) }
  end

  factory :nozip_course_creation_customcontent, class: 'CourseCreation::CustomContent' do
    name ''
    zip ''
  end
end
