FactoryGirl.define do
  factory :course_creation_answer, class: 'CourseCreation::Answer' do
    option Faker::Lorem.sentence
    correct_answer Faker::Number.between(0, 1)
  end
end
