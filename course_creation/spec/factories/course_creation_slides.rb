require 'pry'
FactoryGirl.define do
  factory :slide, class:
    'CourseCreation::Slide' do |s|
    presentation
    s.title Faker::Name.title
    s.number_of_columns Faker::Number.number(1)
  end
end
