FactoryGirl.define do
  factory :course_creation_question_category,
          class: 'CourseCreation::QuestionCategory' do |p|
    p.name Faker::Name.title
  end
end
