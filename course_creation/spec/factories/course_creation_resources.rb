require 'spec_helper'
FactoryGirl.define do
  factory :resource, class: 'CourseCreation::Resource' do
    association :version,
                factory: :course_creation_version,
                strategy: :create
    title Faker::Commerce.product_name

    factory :video do
      file { Rack::Test::UploadedFile.new(File.join(CourseCreation::Engine.root, '/spec/fixtures/test.mp4')) }
      type 'CourseCreation::Video'
    end

    factory :document do
      file { Rack::Test::UploadedFile.new(File.join(CourseCreation::Engine.root, '/spec/fixtures/test.pdf')) }
      type 'CourseCreation::Document'
    end

    factory :quote do
      content Faker::Lorem.sentence
      type 'CourseCreation::Quote'
    end

    factory :note do
      content Faker::Lorem.paragraph
      type 'CourseCreation::Note'
    end
  end
end
