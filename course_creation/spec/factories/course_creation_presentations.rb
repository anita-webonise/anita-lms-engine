FactoryGirl.define do
  factory :presentation, class:
    'CourseCreation::Presentation' do |p|
    p.title Faker::Name.title
    association :course_section,
                factory: :course_creation_course_section,
                strategy: :create
  end

  factory :invalid_presentation, class:
    'CourseCreation::Presentation' do |p|
    p.title nil
    association :course_section,
                factory: :course_creation_course_section,
                strategy: :create
  end
end
