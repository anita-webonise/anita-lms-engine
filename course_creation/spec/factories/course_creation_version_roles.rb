FactoryGirl.define do
  factory :course_creation_version_roles,
          class: 'CourseCreation::VersionRole' do
    before(:create) do |v|
      course = FactoryGirl.create(:course_creation_course)
      version = FactoryGirl.create(:course_creation_version,
                                   course_id: course.id)
      role = FactoryGirl.create(:course_creation_role)
      v.version_id = version.id
      v.role_id = role.id
    end
  end
end
