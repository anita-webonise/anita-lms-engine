FactoryGirl.define do
  factory :course_creation_user_course_evaluation,
          class: 'CourseCreation::CourseTaking::UserCourseEvaluation' do
    association :user_course,
                factory: :course_creation_user_courses,
                strategy: :create
  end
end
