require File.expand_path('../../../rails_helper', __FILE__)
require 'spec_helper'
# course creation module
module CourseCreation
  describe CategoriesController do

    routes { CourseCreation::Engine.routes }

    describe 'GET #index' do
      let(:category) { FactoryGirl.create(:category) }
      it 'renders the :index view' do
        get :index
        response.should render_template :index
      end
      it 'populates an array of categories' do
        get :index
        assigns(:categories).should eq([category])
      end
    end

    describe 'GET #show' do
      let(:category) { FactoryGirl.create(:category) }
      it 'renders the :show template' do
        get :show, id: category.id
        response.should render_template :show
      end
      it 'assigns the requested category to @category' do
        get :show, id: category.id
      end
    end

    describe 'GET #new' do
      it 'assigns a new Category to @category' do
        expect do
          post :create, category: FactoryGirl.attributes_for(:category)
        end.to change(Category, :count).by(1)
      end
    end

    describe 'POST #create' do
      context 'with valid attributes' do
        it 'redirects to the category index' do
          post :create, category: FactoryGirl.attributes_for(:category)
          response.should redirect_to categories_path
        end
      end
      context 'with invalid attributes' do
        it 'does not save the new category' do
          expect do
            post :create,
                 category: FactoryGirl.attributes_for(:invalid_category)
          end.to_not change(Category, :count)
        end
        it 'redirects to index' do
          post :create, category: FactoryGirl.attributes_for(:invalid_category)
          response.should render_template :new
        end
      end
    end

    describe 'DELETE destroy' do
      let!(:category) { FactoryGirl.create(:category) }
      it 'redirects to category#index' do
        get :index
        response.should render_template :index
      end
      it 'deletes the category' do
        expect do
          delete :destroy, id: category
        end.to change(Category, :count).by(-1)
      end
    end
  end
end
