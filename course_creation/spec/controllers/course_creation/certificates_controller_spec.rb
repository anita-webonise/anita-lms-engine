require 'rails_helper'

module CourseCreation
  describe CertificatesController do
    routes { CourseCreation::Engine.routes }

    describe 'GET #index' do
      let(:certificate) { FactoryGirl.create(:course_creation_certificate) }
      it 'renders the :index view' do
        get :index
        response.should render_template :index
      end
    end

    describe 'GET #new' do
      it 'renders the :new template' do
        get :new
        response.should render_template :new
      end
    end

    describe 'POST #create' do
      context 'with valid attributes' do
        it 'creates a new certificate' do
          expect do
            post :create, certificate:
                 attributes_for(:course_creation_certificate)
          end.to change(Certificate, :count).by(1)
        end
        it 'redirects to the index' do
          post :create, certificate:
               attributes_for(:course_creation_certificate)
          response.should redirect_to certificates_path
        end
      end
      context 'with invalid attributes' do
        it 'does not save the new category' do
          expect do
            post :create, certificate:
                 attributes_for(:course_creation_certificate, file: nil)
          end.to_not change(Certificate, :count)
        end
        it 're-renders the :new template' do
          post :create, certificate:
               attributes_for(:course_creation_certificate, file: nil)
          response.should render_template :new
        end
      end

      context 'with correct course associations' do
        let(:course_1) { create :course_creation_course }
        let(:course_2) { create :course_creation_course }
        let(:course_3) { create :course_creation_course }

        it 'associates to the correct courses' do
          post :create, certificate:
               attributes_for(:course_creation_certificate,
                              course_ids: [course_1.id, course_2.id])
          courses = Certificate.last.courses
          expect(courses).to include(course_1)
          expect(courses).to include(course_2)
        end

        it 'replaces old certificates with new association' do
          post :create, certificate:
               attributes_for(:course_creation_certificate,
                              course_ids: [course_1.id, course_2.id])
          courses_1 = Certificate.last.courses
          expect(courses_1).to include(course_1)
          expect(courses_1).to include(course_2)

          post :create, certificate:
               attributes_for(:course_creation_certificate,
                              course_ids: [course_2.id, course_3.id])
          courses_2 = Certificate.last.courses
          expect(courses_2).to include(course_2)
          expect(courses_2).to include(course_3)

          expect(courses_1.reload).not_to include(course_2)
        end
      end
    end

    describe 'PUT update' do
      let!(:certificate) { create :course_creation_certificate }
      it 'update the requested @certificates' do
        new_title = Faker::Name.name
        put :update, id: certificate, certificate: { title: new_title }
        expect(certificate.reload.title).to eq(new_title)
      end
    end

    describe 'DELETE destroy' do
      let!(:certificate) { FactoryGirl.create(:course_creation_certificate) }
      it 'deletes the certificate' do
        expect do
          delete :destroy, id: certificate.id
        end.to change(Certificate, :count).by(-1)
      end
    end
  end
end
