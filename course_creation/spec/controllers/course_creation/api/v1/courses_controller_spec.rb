require 'spec_helper'
module CourseCreation
  module API
    # Courses controller test cases
    module V1
      describe CoursesController do
        describe 'GET #show', type: :request do
          let(:course) { FactoryGirl.create(:course_creation_course) }
          it 'should return success for valid data' do
            get api_v1_course_path(course.id)
            expect(response).to be_success
          end

          it 'should return 404 for invalid data' do
            get api_v1_course_path(-1)
            status = JSON.parse(response.body)['status']['code']
            expect(status).to eq(404)
          end

          it 'should return valid data' do
            version = FactoryGirl.create_list(:course_creation_version,
                                              1, course_id: course.id)
            get api_v1_course_path(course.id)
            parsed_response = JSON.parse(response.body)['data']
            expect(parsed_response['id']).to eq(course.id)
            expect(parsed_response['title']).to eq(course.title)
            expect(parsed_response['versions'][0]['id']
                  ).to eq version.first.id
            expect(parsed_response['versions'][0]['description']
                  ).to eq version.first.description
          end
        end
      end
    end
  end
end
