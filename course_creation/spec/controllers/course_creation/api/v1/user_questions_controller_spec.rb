require 'spec_helper'
module CourseCreation
  module API
    # UserCourses controller test cases
    module V1
      describe  UserQuestionsController do
        describe 'POST #create', type: :request do
          let(:user_question) { create(:course_creation_user_question) }
          let(:question_category) { create :course_creation_question_category  }

          it 'should return success for objective valid data' do
            answer = FactoryGirl.create_list(:course_creation_answer,
                                               5, question_id: user_question.question_id)
            post api_v1_user_questions_path,
            question: { question_id: user_question.question_id,
                        question_type_id: question_category.id,
                        assessment_id: user_question.assessment_id,
                        assessment_type: user_question.assessment_type,
                        user_course_id: user_question.user_course_id,
                        objective_answers: [answer_id: answer.first.id,
                        is_correct_answer: true]
                         }
            expect(response).to be_success
          end

          it 'should return success for descriptive valid data' do
            answer = FactoryGirl.create_list(:course_creation_answer,
                                               5, question_id: user_question.question_id)
            post api_v1_user_questions_path,
            question: { question_id: user_question.question_id,
                        question_type_id: question_category.id,
                        assessment_id: user_question.assessment_id,
                        assessment_type: user_question.assessment_type,
                        user_course_id: user_question.user_course_id,
                        objective_answers: [answer: answer.first,
                        is_correct_answer: false]
                         }
            expect(response).to be_success
          end
        end
      end
    end
  end
end
