require 'spec_helper'
module CourseCreation
  module API
    # UserCourses controller test cases
    module V1
      describe UserCoursesController do
        describe 'GET #show', type: :request do
          let(:user_course) do
            FactoryGirl.create(:course_creation_user_courses)
          end
          it 'should return success for valid data' do
            get api_v1_user_course_path(user_course.id)
            expect(response).to be_success
          end

          it 'should return 404 for invalid data' do
            get api_v1_user_course_path(-1)
            status = JSON.parse(response.body)['status']['code']
            expect(status).to eq(404)
          end

          it 'should return valid data' do
            get api_v1_user_course_path(user_course.id)
            parsed_response = JSON.parse(response.body)['data']
            expect(parsed_response['id']).to eq user_course.id
            expect(parsed_response['progress_percentage']
                  ).to eq user_course.progress_percentage.to_s
            expect(parsed_response['version_id']).to eq user_course.version_id
          end
        end
      end
    end
  end
end
