require 'spec_helper'
module CourseCreation
  module API
    # UserCourses controller test cases
    module V1
      describe UserCourseAssessmentsController do
        let(:assessment) { FactoryGirl.create(:course_creation_assessment) }
        let(:user_course) do
          FactoryGirl.create(:course_creation_user_courses)
        end
        describe 'POST #create', type: :request do
          it 'should return success for valid data' do

            post api_v1_user_course_assessments_path,
                 user_course_assessment: { assessment_id: assessment.id,
                                           user_course_id: user_course.id,
                                           assessment_percentage: 100,
                                           status: 1 }
            expect(response).to be_success
          end

          it 'should return 402 for parameter missing' do
            post api_v1_user_course_assessments_path,
                 user_course_assessment: { assessment_id: assessment.id,
                                           assessment_percentage: 100 }
            status = JSON.parse(response.body)['status']['code']
            expect(status).to eq(402)
          end
        end
      end
    end
  end
end
