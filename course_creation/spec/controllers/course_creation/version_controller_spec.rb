require File.expand_path('../../../rails_helper', __FILE__)
require 'spec_helper'
# course creation module
module CourseCreation
  describe VersionsController do
    describe 'GET #index' do
      routes { CourseCreation::Engine.routes }

      let(:version) { FactoryGirl.create(:course_creation_course) }
      it 'renders the :index view' do
        get :index
        expect(response).to render_template :index
      end

      it 'populates an array of course versions' do
        get :index
        expect(:version).not_to be_empty
      end

      it 'should return results' do
        search_text = Faker::Name.name
        FactoryGirl.create(:course_creation_course, title: search_text)
        get :index, 'search' => search_text
        response.should be_ok
        expect(:version).not_to be_empty
      end
    end

    describe 'DELETE #destroy', type: :request do
      context 'With unpublished version' do
        let!(:version_unpublished) { FactoryGirl.create(:unpublish_version) }
        it 'deletes the version' do
          expect do
            delete version_path(version_unpublished)
          end.to change(Version, :count).by(-1)
        end
      end
      context 'With published version' do
        let!(:version_published) { FactoryGirl.create(:publish_version) }
        it 'deletes the version' do
          expect do
            delete version_path(version_published)
          end.to_not change(Version, :count)
        end
      end
    end
  end
end
