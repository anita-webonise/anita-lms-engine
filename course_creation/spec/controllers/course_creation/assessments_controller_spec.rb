require 'rails_helper'
# CourseCreation
module CourseCreation
  RSpec.describe AssessmentsController, type: :controller do
    routes { CourseCreation::Engine.routes }
    describe do
      let(:practice_assessment) { FactoryGirl.create(:course_creation_practice_assessment) }
      let(:quiz_assessment) { FactoryGirl.create(:course_creation_quiz_assessment) }
      it 'renders the :index view' do
        get :index
        response.should render_template :index
      end
      it 'populates an array of assessments' do
        get :index
        expect(:assessment).not_to be_empty
      end
      it 'populates an array of assessments' do
        get :index
        expect(:practice_assessment).not_to be_empty
      end
      let(:quiz_assessment) { FactoryGirl.create(:course_creation_passing_quiz_assessment) }
      it 'renders the :index view' do
        get :index
        response.should render_template :index
      end
      it 'populates an array of assessments' do
        get :index
        expect(:quiz_assessment).not_to be_empty
      end
    end
    let (:version) { create :course_creation_version }
    let (:course_section) { create :course_creation_course_section }

    describe 'GET #new' do
      it 'assigns a new Assessment to @assessment' do
        get :new, version_id: version.id
      end
    end
    describe 'POST #create' do
      context 'with valid attributes' do
        it 'redirects to the index view' do
          expect do
            post :create, assessment: attributes_for(:course_creation_quiz_assessment),
                          version_id: version.id,
                          course_section_id: course_section.id

          end.to change(Assessment, :count).by(1)
        end
      end
    end
  end
end
