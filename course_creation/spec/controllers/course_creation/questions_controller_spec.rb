require File.expand_path('../../../rails_helper', __FILE__)
require 'spec_helper'
# course creation module
module CourseCreation
  describe QuestionsController do

    routes { CourseCreation::Engine.routes }

    let(:assessment) do
      FactoryGirl.create(:course_creation_quiz_assessment)
    end
    let(:question) do
      FactoryGirl.create(:course_creation_question,
                         assessment: assessment)
    end
    let(:answer) do
      FactoryGirl.create(:course_creation_answer,
                         question_id: :question.id)
    end

    describe 'GET #new' do
      it 'assigns a new Course to @question' do
        get :new, assessment_id: assessment.id
      end
    end

    describe 'POST #create' do
      context 'with valid attributes' do
        it 'redirects to the new question' do
          expect do
            answer_attributes = {}
            answer_attributes['0'] = { option: Faker::Lorem.sentence,
                                       correct_answer:
                                       Faker::Number.between(0, 1) }
            answer_attributes['1'] = { option: Faker::Lorem.sentence,
                                       correct_answer:
                                       Faker::Number.between(0, 1) }

            post :create, { question: FactoryGirl.attributes_for(
              :course_creation_question, answers_attributes: answer_attributes,
                                         assessment_id: assessment.id
            ), assessment_id: assessment.id }
          end.to change(Question, :count).by(1)
        end

        it 'question for visual and redirection to the new question' do
          expect do
            answer_attributes = {}
            file_upload = fixture_file_upload('test.jpg', 'image/jpg')
            answer_attributes['0'] = { image: file_upload,
                                       correct_answer:
                                       Faker::Number.between(0, 1) }
            answer_attributes['1'] = { image: file_upload,
                                       correct_answer:
                                       Faker::Number.between(0, 1) }
            post :create, { question: FactoryGirl.attributes_for(
              :course_creation_question, answers_attributes: answer_attributes,
                                         assessment_id: assessment.id
            ), assessment_id: assessment.id }
          end.to change(Question, :count).by(1)
        end
      end
    end

    describe 'DELETE destroy' do
      it 'deletes the question' do
        delete :destroy, assessment_id: question.assessment_id,
                         id: question.id
        expect(Question.where(id: question.id).count).to eq(0)
      end
    end
  end
end
