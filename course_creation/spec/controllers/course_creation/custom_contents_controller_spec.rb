require 'rails_helper'
# course creation module
module CourseCreation
  RSpec.describe CustomContentsController, type: :controller do
    let(:version) { create :course_creation_version }
    let(:chapter) { create :course_creation_course_section, version_id: version.id }
    let(:customcontent) { create :course_creation_customcontent,
                                 version_id: version.id,
                                 course_section_id: chapter.id}

    routes { CourseCreation::Engine.routes }

    describe do
      it 'renders the :index view' do
        get :index, version_id: version.id, course_section_id: chapter.id
        response.should render_template :index
      end
      it 'populates an array of customcontents' do
        get :index, version_id: version.id, course_section_id: chapter.id
        expect(:customcontent).not_to be_empty
      end
    end

    describe 'GET #new' do
      it 'assigns a new Course to @course' do
        get :new, version_id: version.id, course_section_id: chapter.id
      end
    end
  end
end
