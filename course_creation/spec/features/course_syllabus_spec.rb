require 'spec_helper'
require 'rails_helper'

module CourseCreation
  feature 'Course can be modified on syllabus page' do
    before(:each) do
      @course = create(:course_creation_course_versions)
      @version = @course.versions.first
      @chapters = create_list(:course_creation_course_section, 5, version_id: @version.id)
      visit syllabus_version_path(@version.id)
    end

    scenario 'course chapters are displayed on page' do
      @chapters.each { |ch| page.should have_content(ch.name) }
    end

    scenario 'course chapters can be added', js: true do
      first('.add-new-chapter-btn').click
      page.has_field? ('Name')
      page.fill_in('course_section_name', with: "Chapter S")
      click_button "Save"
      page.should have_content("Chapter S")
    end
  end
end
