require File.expand_path('../../../rails_helper', __FILE__)
require 'spec_helper'
# course creation module for category
module CourseCreation
  describe Category do
    it 'is invalid without a name' do
      FactoryGirl.build(:category, name: nil).should_not be_valid
    end
    it 'returns a contact\'s full name as a string' do
      FactoryGirl.create(:category, name: 'test').name.should == 'test'
    end
  end
end
