require 'rails_helper'

module CourseCreation
  RSpec.describe EvaluationQuestion, type: :model do
    it 'is invalid without content' do
      build(:course_creation_evaluation_question, content: nil).should_not be_valid
    end
  end
end
