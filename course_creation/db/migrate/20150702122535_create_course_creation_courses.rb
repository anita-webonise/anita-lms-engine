# course creation courses migration
class CreateCourseCreationCourses < ActiveRecord::Migration
  def change
    create_table :course_creation_courses do |t|
      t.string :title
      t.timestamps null: false
    end
  end
end
