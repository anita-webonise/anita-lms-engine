class AddForeignKeyToCustomContent < ActiveRecord::Migration
  def change
    add_foreign_key :course_creation_custom_contents,
                    :course_creation_course_sections,
                    column: :course_section_id
    add_index :course_creation_custom_contents, :course_section_id
  end
end
