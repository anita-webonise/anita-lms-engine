# Remove description attribute from customcontent
class RemoveDescriptionFromCourseCreationCustomContent < ActiveRecord::Migration
  def change
    remove_column :course_creation_custom_contents, :description, :text
  end
end
