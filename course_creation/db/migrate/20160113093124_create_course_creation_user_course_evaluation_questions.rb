# UserCourseEvaluationQuestions migration
class CreateCourseCreationUserCourseEvaluationQuestions < ActiveRecord::Migration
  def change
    create_table :course_creation_user_course_evaluation_questions do |t|
      t.integer :evaluation_question_id
      t.integer :answer_id
      t.integer :user_course_evaluation_id
    end
    add_foreign_key :course_creation_user_course_evaluation_questions,
                    :course_creation_user_course_evaluations,
                    column: :user_course_evaluation_id
  end
end
