# adding not null to assessment_type in assessment
class Setnotnullassessmenttype < ActiveRecord::Migration
  def change
    change_column :course_creation_assessments,
                  :assessment_type, :string, null: false
  end
end
