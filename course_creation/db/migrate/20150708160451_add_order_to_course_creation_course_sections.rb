class AddOrderToCourseCreationCourseSections < ActiveRecord::Migration
  def change
    add_column :course_creation_course_sections, :course_order, :integer
    add_column :course_creation_course_sections, :chapter_order, :integer
  end
end
