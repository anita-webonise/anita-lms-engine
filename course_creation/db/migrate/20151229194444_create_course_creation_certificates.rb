class CreateCourseCreationCertificates < ActiveRecord::Migration
  def change
    create_table :course_creation_certificates do |t|
      t.string :title
      t.string :file
      t.text :description

      t.timestamps null: false
    end
  end
end
