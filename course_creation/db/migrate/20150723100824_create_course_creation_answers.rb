# create table for answers
class CreateCourseCreationAnswers < ActiveRecord::Migration
  def change
    create_table :course_creation_answers do |t|
      t.string :option
      t.string :image
      t.timestamps null: false
    end
  end
end
