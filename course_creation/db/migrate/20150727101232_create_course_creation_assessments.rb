# coursecreation for assessment
class CreateCourseCreationAssessments < ActiveRecord::Migration
  def change
    create_table :course_creation_assessments do |t|
      t.string :name, null: false
      t.text :description
      t.string :assessment_type
      t.string :passing_criteria
      t.float :passing_percentage, inclusion: 0..100, default: 1
      t.integer :no_of_displayed_questions
      t.integer :course_section_id
      t.boolean :upfront
      t.boolean :additional_text
      t.boolean :details_page
      t.boolean :randomize
      t.timestamps null: false
    end
    add_foreign_key :course_creation_assessments,
                    :course_creation_course_sections,
                    column: :course_section_id
    add_index :course_creation_assessments, :course_section_id
  end
end
