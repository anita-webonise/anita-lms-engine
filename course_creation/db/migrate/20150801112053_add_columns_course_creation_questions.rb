# Add columns to table
class AddColumnsCourseCreationQuestions < ActiveRecord::Migration
  def change
    add_column :course_creation_questions, :test_reference_section_id, :integer
    add_column :course_creation_questions, :test_reference_page_id, :integer
    add_index :course_creation_questions, :test_reference_section_id
    add_index :course_creation_questions, :test_reference_page_id
  end
end
