class AddVersionOrderToCourseCreationResources < ActiveRecord::Migration
  def change
    add_column :course_creation_resources, :version_order, :integer
  end
end
