# Changing Column name in custom content
class CustomContentChangeColumnName < ActiveRecord::Migration
  def change
    rename_column :course_creation_custom_contents, :content_id, :course_section_id
  end
end
