# rename column for interactive slides
class RenameColumnForInteractiveSlides < ActiveRecord::Migration
  def change
    rename_column :course_creation_interactive_slides, :section_id,
                  :course_section_id
  end
end
