# User course assessment table
class CreateCourseCreationUserCourseAssessments < ActiveRecord::Migration
  def change
    create_table :course_creation_user_course_assessments do |t|
      t.integer :user_course_id
      t.integer :assessment_id
      t.integer :status
      t.integer :assessment_percentage
      t.string :assessment_type
      t.timestamps null: false
    end
    add_foreign_key :course_creation_user_course_assessments,
                    :course_creation_user_courses,
                    column: :user_course_id
    add_index :course_creation_user_course_assessments, :user_course_id
    add_foreign_key :course_creation_user_course_assessments,
                    :course_creation_assessments,
                    column: :assessment_id
  end
end
