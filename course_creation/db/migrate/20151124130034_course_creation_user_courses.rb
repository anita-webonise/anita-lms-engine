# User course table
class CourseCreationUserCourses < ActiveRecord::Migration
  def change
    create_table :course_creation_user_courses do |t|
      t.integer :user_id
      t.integer :version_id
      t.timestamps :start_date
      t.timestamps :completion_date
      t.timestamps :purchase_date
      t.integer :course_status
      t.string :last_item_id
      t.string :last_item_type
      t.integer :progress_percentage
      t.integer :course_score
      t.timestamps null: false
    end
    add_foreign_key :course_creation_user_courses,
                    :course_creation_versions,
                    column: :version_id
    add_index :course_creation_user_courses, :version_id
  end
end
