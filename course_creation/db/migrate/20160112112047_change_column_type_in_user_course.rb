# Change progress percentage column type to decimal
class ChangeColumnTypeInUserCourse < ActiveRecord::Migration
  def change
    change_column :course_creation_user_courses, :progress_percentage,
                  :decimal, precision: 3, scale: 2
  end
end
