# User course progress table
class CreateCourseCreationUserCourseProgresses < ActiveRecord::Migration
  def change
    create_table :course_creation_user_course_progresses do |t|
      t.integer :content_id
      t.integer :user_course_id
      t.integer :status
      t.string :type
      t.integer :attempt_to
      t.timestamps null: false
    end
    add_foreign_key :course_creation_user_course_progresses,
                    :course_creation_user_courses,
                    column: :user_course_id
    add_index :course_creation_user_course_progresses, :user_course_id
  end
end
