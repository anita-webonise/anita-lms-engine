# Add order column to slides
class AddOrderToCourseCreationSlides < ActiveRecord::Migration
  def change
    add_column :course_creation_slides, :slides_order, :integer
  end
end
