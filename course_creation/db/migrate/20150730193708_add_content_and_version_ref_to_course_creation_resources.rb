class AddContentAndVersionRefToCourseCreationResources < ActiveRecord::Migration
  def change
    add_column :course_creation_resources, :content, :text
    add_column :course_creation_resources, :version_id, :integer
    add_foreign_key :course_creation_resources,
                    :course_creation_versions,
                    column: :version_id
    add_index :course_creation_resources, :version_id
  end
end
