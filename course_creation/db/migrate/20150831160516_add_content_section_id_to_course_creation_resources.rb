class AddContentSectionIdToCourseCreationResources < ActiveRecord::Migration
  def change
    add_column :course_creation_resources, :content_section_id, :integer
    add_foreign_key :course_creation_resources,
                    :course_creation_course_sections,
                    column: :content_section_id
    add_index :course_creation_resources, :content_section_id
  end
end
