# create table for question categories
class CreateCourseCreationQuestionCategories < ActiveRecord::Migration
  def change
    create_table :course_creation_question_categories do |t|
      t.string :name
    end

    %w(multiple_answers single_answer true_or_false
       single_visual multiple_visuals match_the_pairs
       multiple_matching descriptive sorting).each do |categoryname|
      CourseCreation::QuestionCategory.create!(name: categoryname)
    end
  end
end
