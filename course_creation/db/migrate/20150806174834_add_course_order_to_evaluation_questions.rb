class AddCourseOrderToEvaluationQuestions < ActiveRecord::Migration
  def change
    add_column :course_creation_evaluation_questions, :course_order, :integer
  end
end
