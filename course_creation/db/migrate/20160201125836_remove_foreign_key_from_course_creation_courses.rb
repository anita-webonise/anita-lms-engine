# Remove Foreign Key From Course Creation Courses
class RemoveForeignKeyFromCourseCreationCourses < ActiveRecord::Migration
  def change
    remove_foreign_key :course_creation_courses, column: :certificate_id
  end
end
