# course creation versions migration
class CreateCourseCreationVersions < ActiveRecord::Migration
  def change
    create_table :course_creation_versions do |t|
      t.string :version
      t.text :description
      t.string :image
      t.string :video
      t.integer :expiry
      t.boolean :price
      t.string :default_image
      t.integer :prerequisite
      t.integer :course_id
      t.integer :category_id
      t.timestamps null: false
    end
  end
end
