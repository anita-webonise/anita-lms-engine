# add editable column to versions
class AddEditableColumnToCourseCreationVersions < ActiveRecord::Migration
  def change
    add_column :course_creation_versions, :editable, :boolean, default: true
  end
end
