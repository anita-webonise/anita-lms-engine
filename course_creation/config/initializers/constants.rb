DEFAULT_EXPIRY = { '1 year from the date of user purchase' => '365',
                  '6 months from the date of user purchase' => '180',
                  '3 months from the date of user purchase' => '90' }

EVALUATION_RESPONSES = ['Strongly Agree',
                        'Somewhat Agree',
                        'Neither Agree nor Disagree',
                        'Somewhat Disagree',
                        'Strongly Disagree']

RESOURCE_TYPES = ['CourseCreation::Video',
                  'CourseCreation::Document',
                  'CourseCreation::Quote',
                  'CourseCreation::Note']

MAX_MULTIPLE_MATCH = 10

MIN_MULTIPLE_MATCH = 1

ACTIVE_CONTROLLERS =
  %w(assessments questions custom_contents evaluation_questions
     interactive_slides interactive_slides_informations)

ACTIVE_CLASS = 'active'

CATEGORY_TAB = 'categories'

CERTIFICATES_TAB = 'certificates'

MAX_QUESTION_OPTION = 14

MIN_QUESTION_OPTION = 3

IMAGE_FILE_TYPE = 'jpeg|jpg|png'

VIDEO_FILE_TYPE = 'mp4'

MAX_TEXT_IMAGE_SLIDE = 6

MAX_IMAGE_SLIDE = 6

DESCRIPTIVE_QUESTION_TYPE = ['match_the_pairs',
                  'multiple_matching',
                  'descriptive']

OBJECTIVE_QUESTION_TYPE = ['single_answer',
                  'single_visual',
                  'multiple_answers',
                  'multiple_visuals',
                  'true_or_false']

S3_BUCKET = Rails.application.secrets.bucket_name
