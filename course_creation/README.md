### Ruby Version
    ruby 2.2.0
### Rails Version
    4.2.1
### Database
    PostgreSQL

### Setup

    1) Clone from the current repository.
      git@github.com:webonise/webo-lfe.git

    2) Get the latest code from *develop* branch of the repository.

    3) Add the database.yml in *test/dummmy/config/* directory.
      sample `database.dist.yml`

    5)Install pre-commit

      The pre-commit gem prevents you from committing some forms of bad code:

      gem install pre-commit
      bundle exec pre-commit install

      Check out the [documentation](https://github.com/jish/pre-commit) for available checks. We have a set of defaults set up in config/pre_commit.yml.

### Install Instructions
    1) Add `gem 'course_creation', git: 'git@github.com:webonise/webo-lfe.git'` to your Gemfile
    2) Run `bundle install`
    3) Run `rake course_creation:install:migrations`
    4) Run `rake db:migrate`


### Git Convetions
    1) Branch Naming convention:
        branch_type/user_initials/branch_name

      Branch Type for:
        Adding New Feature:
          feature (branches from develop, merges to develop)

        Hotfix on production:
          hotfix (branches from master, merges to master and develop)

        Release Branch
        (Contains all the features for next release to production.
         Can be used for minor bugfixes before next production release):
          release (branches from develop, merges to master and develop)

        Other Bugfixes:
          bugfix (branches from develop, merges to develop)

      For Example:
        feature/mn/add_admin_functionality

    2) Rebase from parent branch before sending a PR

    3) Add `Pending Review` label to the PR after creating it.

    5) Reviewer should remove `Pending Review` label and Merge after:
        - the developer has resolved the PR comments and
        - the PR has received 1 thumbs up from him/her.

    6) Pull Request Format:
        PR should contain the following sections:
          Jira Ticket Link:
          Problem/ Requirement Description:
          Solution implemented:
          QA instructions:

    7) Reviewer should use `Needs Rebase` label to indicate to the developer that the PR is not rebased with the latest code on the parent branch.

### Note
      1) Specify gem version while adding new gem
      2) Add dependency in course_creation.gemspec
      3) Integrate with host application and test

