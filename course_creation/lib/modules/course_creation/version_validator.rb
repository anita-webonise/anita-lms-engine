module CourseCreation
  # validate course editable or no
  module VersionValidator
    def version_published
      return true unless current_version.published
      errors[:base] << I18n.t(
        'course_creation.edit_content_err_msg'
      ) unless errors.any?
      false
    end

    def version_editable
      return if current_version.editable && version_published
      errors[:base] << I18n.t(
        'course_creation.edit_content_purchased_course'
      ) unless errors.any?
      false
    end

    def current_version
      case (self.class.name.split('::').last)
      when 'CourseSection'
        version
      when 'Slide'
        presentation.course_section.version
      when 'Presentation', 'Assessment', 'Resource', 'Video', 'Document'
        course_section ? course_section.version : content_section.version
      when 'Question'
        assessment.course_section.version
      when 'Answer'
        question.assessment.course_section.version
      end
    end
  end
end
