require 'course_creation/engine'
# CourseCreation
module CourseCreation
  # CustomContent
  module Custom
    def passing_zip(customcontent, zip, id)
      dest = FileUtils.mkdir_p("#{Rails.root}/unzip/tmp/#{id}")
      Zip::File.open(zip.path) do |zip_file|
        zip_file.each do |f|
          f_path = File.join(dest, f.name)
          FileUtils.mkdir_p(File.dirname(f_path))
          zip_file.extract(f, f_path)
        end
      end
      if File.file?("#{dest[0]}/#{zip.original_filename[0..-5]}/index.html")
        filepresent(customcontent, dest, zip)
      else
        FileUtils.rm_rf(dest)
        return false
      end
    end

    def filepresent(customcontent, dest, zip)
      if S3_BUCKET
        FileUtils.mv("#{dest[0]}",
                     File.dirname(customcontent.zip.path),
                     verbose: true, force: true)
      else
        FileUtils.mv("#{dest[0]}/#{zip.original_filename[0..-5]}",
                     File.dirname(customcontent.zip.path),
                     verbose: true, force: true)
      end
      FileUtils.rm_rf(dest)
      FileUtils.rm_rf(customcontent.zip.path[0..-5])
      evaluate_zip(customcontent, zip)
    end

    def evaluate_zip(customcontent, name)
      carrier_dir = name.original_filename[0..-5].gsub(/\W/, '_')
      old_dir = File.basename(customcontent.zip.path)[0..-5].gsub(/\W/, '_')
      base_dir = File.dirname(customcontent.zip.path)
      unzip_carrier = FileUtils.mkdir_p "#{base_dir}/#{carrier_dir}"
      orig_dir = File.path("#{base_dir}/#{name.original_filename[0..-5]}")
      FileUtils.copy_entry "#{orig_dir}", "#{unzip_carrier.first}" if
        orig_dir != unzip_carrier.first
      FileUtils.rm_rf("#{orig_dir}") if
        carrier_dir != name.original_filename[0..-5]
      directory = "#{File.dirname(customcontent.zip.path)}/#{old_dir}"
      if File.basename(directory) != carrier_dir
        FileUtils.rm_rf("#{File.dirname(customcontent.zip.path)}/#{old_dir}")
      end
      true
    end
  end
end
