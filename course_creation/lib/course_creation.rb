require 'course_creation/engine'
require 'carrierwave'

# Course Creation
module CourseCreation
  mattr_accessor :user_class, :admin_email, :preview_user_id, :logout_url,
                 :dashboard_url

  CourseCreation.preview_user_id = 1 if Rails.env.development?
end
